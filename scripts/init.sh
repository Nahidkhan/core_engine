#!/bin/sh

# Check if CONFIG environment variable is defined
if [ "x${CONFIG}" = "x" ]
then

# Use /etc/gnukhata.conf as config file
	CONFIG=/etc/gnukhata.conf
fi
